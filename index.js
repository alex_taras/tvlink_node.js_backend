const express = require('express')
const router = express.Router()

const bodyParser = require('body-parser')

const routesController = require('./api/api')

const app = express()
const port = process.env.PORT || 8000;

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.get('/', (req, res) => res.json('TVLink Server'))

app.listen(port, () => {
    console.log(`[server]: Server is running at https://localhost:${port}`)
})

router.post('/companion', routesController.registerCompanion)
router.get('/companion', routesController.fetchCompanion)

router.post('/client', routesController.registerClient)
router.get('/client', routesController.fetchClient)

router.post('/link', routesController.registerLink)
router.get('/link', routesController.fetchLink)

app.use('/api/v1', router)
