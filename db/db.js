const { json } = require('body-parser')
const { fetchCompanion } = require('../api/api')

const Database = require('better-sqlite3')

let db = new Database(':memory:', { verbose: console.log })

initialize()

function initialize() {
    let clientsSQL = `CREATE TABLE "clients" (
        "identifier"	TEXT UNIQUE,
        "link_code"	TEXT UNIQUE,
        "timestamp"	INTEGER,
        PRIMARY KEY("identifier","link_code")
    )`

    let companionsSQL = `CREATE TABLE "companions" (
        "identifier"	TEXT UNIQUE,
        "timestamp"	INTEGER,
        PRIMARY KEY("identifier")
    )`

    let tokensSQL = `CREATE TABLE "tokens" (
        "companion"	TEXT UNIQUE,
        "token"	TEXT,
        "refresh_token"	TEXT,
        PRIMARY KEY("companion")
    )`

    let linksSQL = `CREATE TABLE "links" (
        "companion"	TEXT UNIQUE,
        "client"	TEXT UNIQUE,
        "timestamp"	INTEGER,
        PRIMARY KEY("companion","client")
    )`

    let clientsStmt = db.prepare(clientsSQL)
    clientsStmt.run()
    let companionsStmt = db.prepare(companionsSQL)
    companionsStmt.run()
    let linksStmt = db.prepare(linksSQL)
    linksStmt.run()
    let tokensStmt = db.prepare(tokensSQL)
    tokensStmt.run()
}

function insertCompanion(identifier, tokens, callback) {
    let token = tokens.token
    let refreshToken = tokens.refreshToken
    let timestamp = Date.now()

    var sql = `INSERT INTO companions (identifier, timestamp) VALUES (@identifier, @timestamp)`
    let companionStmt = db.prepare(sql)
    try {
        companionStmt.run({identifier, timestamp})
        console.log(`[database]: Successfuly inserted into COMPANIONS - ${identifier}`)
        
        sql = 'INSERT INTO tokens (companion, token, refresh_token) VALUES (@identifier, @token, @refreshToken)'
        let tokensStmt = db.prepare(sql)
        try {
            tokensStmt.run({identifier, token, refreshToken})
            console.log(`[database]: Successfuly inserted into TOKENS - ${identifier}`)
            getCompanion(identifier, callback)
        } catch {
            console.log(`[database]: Failed to insert into TOKENS: ${error.message}`)
            return callback(error, null)
        }
    } catch(error) {
        console.log(`[database]: Failed to insert into COMPANIONS: ${error.message}`)
        return callback(error, null)
    }
}

function getCompanion(identifier, callback) {
    let sql = `SELECT companions.identifier as identifier, tokens.token, tokens.refresh_token FROM companions LEFT JOIN tokens ON tokens.companion = companions.identifier WHERE companions.identifier = @identifier`
    let companionsStmt = db.prepare(sql) 
    try {
        let row = companionsStmt.get({identifier})
        callback(null, row)
    } catch(error) {
        console.log(`[database]: Failed to SELECT from COMPANIONS, TOKENS: ${error.message}`)
        return callback(error, null)
    }
}

function insertClient(identifier, linkCode, callback) {
    let timestamp = Date.now()

    let sql = `INSERT INTO clients (identifier, link_code, timestamp) VALUES (@identifier, @linkCode, @timestamp)`
    let clientsStmt = db.prepare(sql)
    try {
        clientsStmt.run({identifier, linkCode, timestamp})
        console.log(`[database]: Successfuly inserted into CLIENTS - ${identifier}`)
        getClient(identifier, callback)
    } catch(error) {
        console.log(`[database]: Failed to insert into CLIENTS: ${error.message}`)
        return callback(error, null)
    }
}

function getClient(identifier, callback) {
    let sql = `SELECT identifier, link_code AS linkCode FROM clients WHERE identifier = @identifier`
    let clientStmt = db.prepare(sql)
    try {
        let row = clientStmt.get({identifier})
        callback(null, row)
    }catch(error) {
        console.log(`[database]: Failed to SELECT from CLIENTS: ${error.message}`)
        return callback(error, null)
    }
}

function insertLink(companionIdentifier, linkCode, callback) {
    let timestamp = Date.now()

    // Get the client for the supplied linkCode
    var sql = `SELECT identifier FROM clients WHERE link_code = @linkCode`
    let clientsStmt = db.prepare(sql)
    try {
        let row = clientsStmt.get({linkCode})
        if (row == null) {
            console.log(`[database]: Client NOT FOUND - ${linkCode}`)

            let error = Error("Client not found")
            error.code = "client_not_found"

            return callback(error, null)
        }

        let clientIdentifier = row.identifier

        sql = `INSERT INTO links (companion, client, timestamp) VALUES (@companionIdentifier, @clientIdentifier, @timestamp)`
        let linksStmt = db.prepare(sql)

        try {
            linksStmt.run({companionIdentifier, clientIdentifier, timestamp})
            getLink(clientIdentifier, callback)
        } catch(error) {
            console.log(`[database]: Failed to INSERT into LINKS: ${error.message}`)
            return callback(error, null)
        }
    } catch(error) {
        console.log(`[database]: Failed to SELECT from CLIENTS: ${error.message}`)
        return callback(error, null)
    }
}

function getLink(clientIdentifier, callback) {
    let sql = `SELECT links.client as identifier, tokens.token, tokens.refresh_token FROM links LEFT JOIN tokens ON tokens.companion = links.companion WHERE links.client = @clientIdentifier`
    let linkStmt = db.prepare(sql)
    try {
        let row = linkStmt.get({clientIdentifier})
        callback(null, row)
    } catch(errot) {
        console.log(`[database]: Failed to SELECT from LINKS: ${error.message}`)
        return callback(error, null)
    }
}

module.exports = {
    registerCompanion: insertCompanion,
    fetchCompanion: getCompanion,

    registerClient: insertClient,
    fetchClient: getClient,

    registerLink: insertLink,
    fetchLink: getLink
}