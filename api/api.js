const { response } = require('express')
const {v4: uuidv4} = require('uuid')

const db = require('../db/db')

exports.registerCompanion = (request, response) => {
    let uuid = uuidv4()

    let tokens = {
        token: request.body.token,
        refreshToken: request.body.refreshToken
    }

    db.registerCompanion(uuid, tokens, (error, result) => {
        if (error) {
            console.log(`[server]: RegisterCompanion: DATABASE ERROR: ${error.message}`)
            return response.status(500).json({
                status: "error",
                message: "server_error"
            })
        }

        console.log(`[server]: RegisterCompanion: ${uuid}`)
        response.status(201).json({
            status: "success",
            result
        })
    })
}

exports.fetchCompanion = (request, response) => { 
    let identifier = request.query.identifier

    db.fetchCompanion(identifier, (error, result) => {
        if (error) {
            console.log(`[server]: FetchCompanion: DATABASE ERROR: ${error.message}`)
            return response.status(500).json({
                status: "error",
                message: "server_error"
            })
        }

        if (result == null) {
            console.log(`[server]: FetchCompanion: NOT FOUND - ${identifier}`)
            return response.status(404).json({
                status: "error",
                message: "not_found"
            })
        }

        console.log(`[server]: FetchCompanion for ${request.query.identifier}`)
        response.status(200).json({
            status: "success",
            result
        })
    })
}

exports.registerClient = (request, response) => {
    const uuid = uuidv4()
    const linkCode = request.body.linkCode

    db.registerClient(uuid, linkCode, (error, result) => {
        if (error) {
            console.log(`[server]: RegisterClient: DATABASE ERROR: ${error.message}`)
            return response.status(500).json({
                status: "error",
                message: "server_error"
            })
        }

        console.log(`[server]: RegisterClient: ${uuid} - ${linkCode}`)
        response.status(201).json({
            status: "success",
            result
        })
    })
}

exports.fetchClient = (request, response) => {
    let identifier = request.query.identifier

    db.fetchClient(identifier, (error, result) => {
        if (error) {
            console.log(`[server]: FetchCLIENT: DATABASE ERROR: ${error.message}`)
            return response.status(500).json({
                status: "error",
                message: "server_error"
            })
        }

        if (result == null) {
            console.log(`[server]: FetchClient: NOT FOUND - ${identifier}`)
            return response.status(404).json({
                status: "error",
                message: "not_found"
            })
        }

        console.l6a9beccfe61bog(`[server]: FetchClient for ${request.query.identifier}`)
        response.status(200).json({
            status: "success",
            result
        })
    })
}

exports.registerLink = (request, response) => {
    const uuid = uuidv4()

    const linkCode = request.body.linkCode
    const companionIdentifier = request.body.identifier

    db.registerLink(companionIdentifier, linkCode, (error, result) => {
        if (error) {
            if (error.code == 'client_not_found' || error.code == 'companion_not_found') {
                console.log(`[server]: RegisterLink: Client or Companion NOT FOUND`)
                return response.status(404).json({
                    status: "error",
                    message: "not_found"
                })
            }

            console.log(`[server]: RegisterLink: DATABASE ERROR: ${error.message}`)
            return response.status(500).json({
                status: "error",
                message: "server_error"
            })
        }

        console.log(`[server]: RegisterLink for ${companionIdentifier} - ${linkCode}`)
        response.status(201).json({
            status: "success",
            result
        })
    })
}

exports.fetchLink = (request, response) => {
    let clientIdentifier = request.query.identifier

    db.fetchLink(clientIdentifier, (error, result) => {
        if (error) {
            console.log(`[server]: DATABASE ERROR: ${error.message}`)
            return response.status(500).json({
                status: "error",
                message: "server_error"
            })
        }

        if (result == null) {
            return response.status(404).json({
                status: "error",
                message: "not_found"
            })
        }

        console.log(`[server]: FetchLink for ${clientIdentifier}`)
        response.status(200).json({
            status: "success",
            result
        })
    })
}